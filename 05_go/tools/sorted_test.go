package tools

import "testing"

func TestSort(t *testing.T) {
	if !Sorted([]int{1, 2, 3, 4, 5}) {
		t.Fatal("is not sorted")
	}

	if Sorted([]int{1, 4, 2, 7, 3, 8, 2, 8, 1, 0}) {
		t.Fatal("is sorted")
	}
}
