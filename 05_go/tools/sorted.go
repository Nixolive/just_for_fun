package tools

//nolint
func Sorted(arr []int) bool {
	if len(arr) < 2 {
		return true
	}

	pre := arr[0]

	for _, n := range arr[1:] {
		if pre > n {
			return false
		}

		pre = n
	}

	return true
}
