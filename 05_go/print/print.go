package myprint

type String interface {
	String() string
}

func Println(values ...interface{}) {
	for _, v := range values {
		switch data := v.(type) {
		case int:
			print(data)
		case float32:
			print(data)
		case string:
			print(data)
		case bool:
			print(data)
		default:
			print(v.(String).String())
		}

		print(" ")
	}

	println()
}
