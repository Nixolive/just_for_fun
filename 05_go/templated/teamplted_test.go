package testtemplated

import (
	"fmt"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestTrigger(t *testing.T) {
	trigger := Trigger{
		Timestamp: time.Now().Unix(),
	}

	Convey("Date", t, func() {
		data := "{{ date .Timestamp }}"

		actual, err := TemplateHandler(data, trigger)
		So(err, ShouldBeEmpty)
		// So(actual, ShouldEqual, time.Unix(trigger.Timestamp, 0).String())
		println("\n\nActual", actual)
	})

	Convey("Decrease", t, func() {
		data := "{{ decrease .Timestamp 300 }}"

		actual, err := TemplateHandler(data, trigger)
		So(err, ShouldBeEmpty)
		So(actual, ShouldEqual, fmt.Sprint(trigger.Timestamp-300))
		println("\n\nActual", actual)
	})

	Convey("Increase", t, func() {
		data := "{{ increase .Timestamp 300 }}"

		actual, err := TemplateHandler(data, trigger)
		So(err, ShouldBeEmpty)
		So(actual, ShouldEqual, fmt.Sprint(trigger.Timestamp+300))
		println("\n\nActual", actual)
	})
}
