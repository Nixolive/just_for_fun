package testtemplated

import (
	"bytes"
	"html/template"
	"time"
)

// timestamp, date, year, month, hours, minutes, seconds
// {{decrease $v.Timestamp 300}} to {{increase $v.Timestamp 300}}

// Trigger ?
type Trigger struct {
	Timestamp int64
}

// Date ?
func Date(t int64) string {
	return time.Unix(t, 0).String()
}

// Decrease ?
func (Trigger) Decrease(timestamp, second int64) int64 {
	return timestamp - second
}

// Increase ?
func (Trigger) Increase(timestamp, second int64) int64 {
	return timestamp + second
}

// TemplateHandler ?
func TemplateHandler(templateText string, trigger Trigger) (string, error) {
	buffer := new(bytes.Buffer)
	funcMap := template.FuncMap{
		"date":     Date,
		"decrease": trigger.Decrease,
		"increase": trigger.Increase,
	}

	// Create a template, add the function map, and parse the text.
	triggerTemplate := template.New("titleTest").Funcs(funcMap)
	triggerTemplate, err := triggerTemplate.Parse(templateText)
	if err != nil {
		return "", err
	}

	// Run the template to verify the output.
	err = triggerTemplate.Execute(buffer, trigger)
	if err != nil {
		return "", err
	}

	return buffer.String(), nil
}
