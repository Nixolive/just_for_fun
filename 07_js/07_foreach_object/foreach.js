const object1 = {
  text: "somestring",
  number: 42,
  what: undefined
};

var s = "";

for (let [key, value] of Object.entries(object1)) {
  if (!s) {
    s += "?";
  } else {
    s += "&";
  }

  if (value === undefined) {
    s += `${key}`;
  } else {
    s += `${key}=${JSON.stringify(value)}`;
  }
}

console.log(s);
