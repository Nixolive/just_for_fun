var assert = require("assert");
var isString = require("../package/is_string");

describe("string", function() {
  it("Check is not string: 1", function() {
    assert.ok(!isString.IsString(1));
  });
  it("Check is string: test phrase", function() {
    assert.ok(isString.IsString("test phrase"));
  });
});

describe("Array", function() {
  describe("#indexOf()", function() {
    it("should return -1 when the value is not present", function() {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});
