#!/bin/sh

# Change global path modules
cd && mkdir .node_modules_global
npm config set prefix=$HOME/.node_modules_global

# clear cache
npm cache clean

# generation default package.json 
npm init --yes
# or custom
npm init

