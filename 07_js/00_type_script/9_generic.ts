// -----------------------------------------------
// ---- GENERIC
{
    const cars: string[] = ["ford", ""]
    const cars2: Array<string> = ["ford", ""]
  
    // -- in function
    interface ILength {
      length: number
    }
    function withCount<T extends ILength>(value: T):{value: T, count:string}{
      return {
        value, count:`Count: ${value.length}`
      }
    }
  
    // -- in classes
    class Collection<T extends number | string | boolean>{
      constructor(private _item:T[] = []){}
  
      add(item: T){
        this._item.push(item)
      }
  
      remove(item: T){
        this._item = this._item.filter(i=> i !== item)
      }
  
      get items():T[]{
        return this._item
      }
    }
  }
  const strings = new Collection<string>(['i','am','strings'])
  strings.add('!')
  