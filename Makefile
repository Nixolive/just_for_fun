
.PHONY:default
default: lint

.PHONY: test
test:
	echo 'mode: atomic' > coverage.txt && go list ./00_codewars/... | xargs -n1 -I{} sh -c 'go test -v -bench=. -covermode=atomic -coverprofile=coverage.tmp {} && tail -n +2 coverage.tmp >> coverage.txt' && rm coverage.tmp

.PHONY: lint
lint:
	cd 00_codewars ; GOGC=50 golangci-lint run --enable-all

.PHONY: install-lint
install-lint:
	# binary will be $(go env GOPATH)/bin/golangci-lint
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh\| sh -s -- -b $(go env GOPATH)/bin v1.21.0 

.PHONY: install-git-lint
install-git-lint:
	curl https://raw.githubusercontent.com/llorllale/go-gitlint/master/download-gitlint.sh > download-gitlint.sh
	chmod +x download-gitlint.sh

.PHONY: install-pre-commit
install-pre-commit:
	python3 -m pip install --user --upgrade pip
	python3 -m pip install --user pre-commit
	python3 -m pip install --user gitlint
	pre-commit install -f
	pre-commit install --hook-type commit-msg

# js
installjs:
	sudo npm i --global chai