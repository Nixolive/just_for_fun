package bubblesort

import (
	"testing"

	"gitlab.com/Nixolive/just_for_fun/05_go/tools"
)

//nolint
func TestSort(t *testing.T) {
	arr := []int{111, 4, 6, 2, 4, 3, 1, 46, 8, 9, 0}
	sort(arr)
	if !tools.Sorted(arr) {
		t.Fatal("is not sorted array")
	}
}
