from typing import Any, Optional, Iterable


class Digit:
    def __init__(self, value, previous=None):
        self.value = value
        self.previous = previous

class BigInt:
    """Целое число, не ограниченное по размеру.

    Работает над связным списком.
    """

    tail: Digit

    def __init__(self, tail) -> None:
        self.tail = tail

    @classmethod
    def from_int(cls, num: int) -> 'BigInt':
        """Получить объект BigInt из целого числа.

        :param num: целое число
        """
        tail = None
        for digit in str(num):
            tail = Digit(int(digit), tail)
        return cls(tail)

    def __iter__(self) -> Iterable[int]:
        cur = self.tail
        while cur.previous:
            yield cur.value
            cur = cur.previous
        yield cur.value

    def to_int(self) -> int:
        """Преобразовать BigInt в int.

        :returns: целое число
        """
        return int(
            ''.join(
                reversed(
                    [str(digit) for digit in self]
                )
            )
        )

    def __add__(self, other) -> 'BigInt':
        # Написать здесь эффективное решение, работающее над списками
        # вместо этой заглушки:
        a = [x for x in self]
        b = [x for x in other]

        if len(a) < len(b):
            a, b = b, a

        arr, t, tmp = [], 0, 0
        for i in range(len(a)):
            if i > len(b) and tmp == 0:
                break
                
            if len(b) > i:
                t = a[i] + b[i] + tmp
            else:
                t = a[i]+tmp

            tmp = 1 if t > 9 else 0
            t %= 10

            arr.append(t)

        if tmp:
            arr.append(1)

        if len(arr) < len(a):
            arr += a[len(arr):]

        out = None
        for a in reversed(arr):
            out = Digit(a, out)

        return BigInt(out)


if __name__ == '__main__':
    s = BigInt.from_int(100) + BigInt.from_int(99)
    print(s.to_int())
    assert s.to_int() == 199
