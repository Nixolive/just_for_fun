#!/bin/sh
docker stop identidock
docker rm identidock
docker rmi identidock
docker build -t identidock .
docker run --name identidock -d -P \
--restart always -v "$(pwd)"/app:/app identidock
sleep 1

IFS=$'\n'
for LINE in $(docker port identidock);
do
    if [[ $LINE == *"9090"* ]]; then curl localhost:"${LINE:20}";
    fi;
done;

#docker run --name identidock -it -p 5000:5000 identidock sh
#docker exec -it identidock sh