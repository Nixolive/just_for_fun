#!/bin/sh

IFS=$'\n'
for LINE in $(docker port identidock);
do
    if [[ $LINE == *"9090"* ]]; then echo localhost:${LINE:20} ; curl localhost:${LINE:20};
    fi;
done;