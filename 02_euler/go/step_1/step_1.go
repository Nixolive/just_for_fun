package main

func main() {
	n := 1000

	for i := 0; i < n; i++ {
		if i%3 == 0 {
			println(i)
		} else if i%5 == 0 {
			println(i)
		}
	}
}
