package main

//nolint
/*
Каждый следующий элемент ряда Фибоначчи получается при сложении двух предыдущих. Начиная с 1 и 2, первые 10 элементов будут:
1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
Найдите сумму всех четных элементов ряда Фибоначчи, которые не превышают четыре миллиона.
*/

//nolint
func main() {
	var data [50]int

	for k := range data {

		switch k + 1 {
		case 1, 2:
			data[k] = k + 1
			continue
		}

		data[k] = data[k-1] + data[k-2]

	}
	var sum int
	for _, v := range data {
		if v%2 == 0 {
			sum += v
		}
	}

	println(sum)
}
